from django.db import models
from django.contrib.auth.models import AbstractUser

class Courses(models.Model):
    name = models.CharField("Nome do curso", max_length=120)

    def __str__(self):
        return self.name

class CustomUser(AbstractUser):
    birth_date = models.DateField(verbose_name="Data de nascimento", blank=True, null=True)
    course = models.ForeignKey(Courses, on_delete=models.CASCADE, blank=True, null=True)

    first_name = None
    last_name = None
    name = models.CharField(max_length=120, blank=True, null=True)
    photo = models.ImageField(blank=True, null=True, upload_to='profile')

    REQUIRED_FIELDS = ["birth_date"]

    def __str__(self):
        return self.username
