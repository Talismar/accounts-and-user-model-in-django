from django.urls import path
from .views import *
from django.contrib.auth.views import LogoutView

urlpatterns = [
    path('profile/', TestView.as_view(), name='profile'),
    path('logout/', LogoutView.as_view(template_name="registration/logout.html"), name="logout"),
    path('signup/', SignupView.as_view(), name="signup"),
    path('change/profile/', ChangeProfileView.as_view(), name="change_profile"),
    path('view/', view_profile, name="view"),
]
