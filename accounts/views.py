from django.shortcuts import render, redirect, get_object_or_404, resolve_url
from django.contrib.auth import update_session_auth_hash
from django.views.generic import CreateView, UpdateView
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView, View
from .forms import *

def signup(request):
    template_name = "registration/signup.html"

    if request.method == "POST":
        form = UserCreation_Form(request.POST)
        if form.is_valid():
            form.save()

            return redirect('profile')
    
    else:
        form = UserCreation_Form()
    
    return render(request, template_name, {"form": form})

class ProfileView(LoginRequiredMixin, TemplateView):

    def get(self, request, *args, **kwargs):
        print(request.user.is_authenticated)
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        if self.request.user.is_authenticated:
            kwargs['name'] = self.request.user.name

        kwargs["title"] = "Profile"

        return super().get_context_data(**kwargs)

class TestView(LoginRequiredMixin, View):
    template_name="profile.html"

    def dispatch(self, request, *args, **kwargs):
        print(request)
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)

class SignupView(CreateView):
    template_name = "registration/signup.html"
    form_class = UserCreation_Form
    success_url = '/accounts/profile/'

    def form_valid(self, form):
        print(form)
        return super().form_valid(form)

def change_profile(request):
    template_name = "registration/change_profile.html"

    if request.method == "POST":
        form = UserChange_Form(request.POST, instance=request.user)

        password_post = {
            "old_password": request.POST.get("old_password"),
            "new_password1": request.POST.get("new_password1"),
            "new_password2": request.POST.get("new_password2")
        }

        password_change_form = PasswordChange_Form(user=request.user, data=password_post)

        if all([form.is_valid(), password_change_form.is_valid()]):
            form.save()

            return redirect('profile')
    
    else:
        form = UserChange_Form(instance=request.user)
        password_change_form = PasswordChange_Form(user=request.user)
    
    context = {
        "form": form,
        "password_change_form": password_change_form
    }

    return render(request, template_name, context)

class ChangeProfileView(UpdateView):
    template_name = "registration/change_profile.html"
    form_class = UserChange_Form
    success_url = '/accounts/profile/'

    def form_valid(self, form):
        context = self.get_context_data()

        try:
            password_change_form = context['password_change_form']
            if password_change_form.is_valid() and form.is_valid():
                form.save()

                is_authenticated = password_change_form.user.is_authenticated

                password_change_form.save()

                """ 
                    Fazendo login do usuario novamente caso esteja mudando do seu perfil
                """
                update_session_auth_hash(self.request, password_change_form.user)

                return redirect(self.get_success_url())
            
        except KeyError:
            if form.is_valid():
                form.save()
                return redirect(self.get_success_url())
            
        return self.render_to_response(self.get_context_data(form=form))

    def form_invalid(self, form):
        return self.render_to_response(self.get_context_data(form=form))

    def get_object(self, queryset=None):
        return self.request.user

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
    
        if self.request.POST:
            context["form"] = self.form_class(self.request.POST, self.request.FILES, instance=self.request.user)

            password_post = {
                                "old_password": self.request.POST.get("old_password"),
                                "new_password1": self.request.POST.get("new_password1"),
                                "new_password2": self.request.POST.get("new_password2")
                            }
            print(self.request.POST, self.request.FILES)
            if password_post["old_password"]:
                print("Password")
                context["password_change_form"] = PasswordChange_Form(user=self.request.user, data=password_post)
        
        else:
            context["password_change_form"] = PasswordChange_Form(user=self.request.user)
        
        return context
    


def view_profile(request):
    pass

""" 
class CreateFatherView(CreateView):
    template_name = 'father_create.html'
    model = Father
    form_class = FatherForm # the parent object's form

    # On successful form submission
    def get_success_url(self):
        return reverse('father-created')

    # Validate forms
    def form_valid(self, form):
        ctx = self.get_context_data()
        inlines = ctx['inlines']
        if inlines.is_valid() and form.is_valid():
            self.object = form.save() # saves Father and Children
            return redirect(self.get_success_url())
        else:
            return self.render_to_response(self.get_context_data(form=form))

    def form_invalid(self, form):
        return self.render_to_response(self.get_context_data(form=form))

    # We populate the context with the forms. Here I'm sending
    # the inline forms in `inlines`
    def get_context_data(self, **kwargs):
        ctx = super(CreateFatherView, self).get_context_data(**kwargs)
        if self.request.POST:
            ctx['form'] = FatherForm(self.request.POST)
            ctx['inlines'] = FatherInlineFormSet(self.request.POST)
        else:
            ctx['form'] = Father()
            ctx['inlines'] = FatherInlineFormSet()
        return ctx
"""