from django.contrib import admin
from .models import CustomUser, Courses


admin.site.register(CustomUser)
admin.site.register(Courses)
