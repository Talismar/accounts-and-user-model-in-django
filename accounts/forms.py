from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm, PasswordChangeForm
from django.contrib.auth import password_validation
from django.utils.translation import gettext_lazy as _
from .models import CustomUser

class UserCreation_Form(UserCreationForm):
    class Meta:
        model = CustomUser
        fields = ['username', 'name', 'photo','birth_date', 'email', 'course', 'password1', 'password2']
        widgets = {
            'birth_date': forms.DateInput(attrs={'type': 'date'}),
        }

class UserChange_Form(UserChangeForm):
    password = None
    username = forms.CharField(help_text="", widget=forms.TextInput(attrs={'readonly': True}))

    class Meta:
        model = CustomUser
        fields = ['username', 'name', 'photo', 'birth_date', 'email', 'course']
        widgets = {
            'birth_date': forms.DateInput(attrs={'type': 'date'}),
        }

class PasswordChange_Form(PasswordChangeForm):
    old_password = forms.CharField(
        label=_("Old password"),
        strip=False,
        required=False,
        widget=forms.PasswordInput(
            attrs={"autocomplete": "current-password", "autofocus": True}
        ),
    )
    new_password1 = forms.CharField(
        label=_("New password"),
        widget=forms.PasswordInput(attrs={"autocomplete": "new-password"}),
        strip=False,
        required=False,
        help_text=password_validation.password_validators_help_text_html(),
    )
    new_password2 = forms.CharField(
        label=_("New password confirmation"),
        strip=False,
        required=False,
        widget=forms.PasswordInput(attrs={"autocomplete": "new-password"}),
    )
    
       